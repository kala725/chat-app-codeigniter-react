<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends MY_Model {

	private $table;

	/**
	 * [__construct Initiates the user table]
	 */
	public function __construct()
	{
		parent::__construct();
		$this->table = 'users';
	}

	/**
	 * [create Creates a user with the given data]
	 * @param  [array] $data [array of elements having information about the created user]
	 * @return [void]
	 */
	public function create( $data ) {
		$data = array(
			'name'       => $data['name'],
			'email'      => $data['email'],
			'password'   => password_hash( $data['password'], PASSWORD_BCRYPT ),
			'created_at' => date('Y-m-j H:i:s'),
		);
		
		$this->db->insert( $this->table, $data );
	}

	/**
	 * [get_by_email Gets the user by email id]
	 * @param  [str] $email [email id]
	 * @return [obj]        [User object matched]
	 */
	public function get_by_email( $email ) {
		$this->db->from($this->table);
		$this->db->where('email', $email);
		return $this->db->get()->row();
	}

	/**
	 * [get Gets the user by ID]
	 * @param  [int] $user_id [User's unique ID]
	 * @return [obj]          [User Object]
	 */
	public function get( $user_id ) {
		$this->db->from($this->table);
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
	}

	/**
	 * [validate_login validates the login credentials and returns the bool flag]
	 * @param  [str] $email    [Email supplied by the user]
	 * @param  [str] $password [Password supplied by the user]
	 * @return [bool]           [Either login is validated or not]
	 */
	public function validate_login( $email, $password ) {
		$hash = $this->db->select('password')
					->from($this->table)
					->where('email', $email)
					->get()->row('password');

		return password_verify($password, $hash);
	}
}