<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_Model extends MY_Model {
	private $table;
	private $chat_room_table;
	private $association_table;

	/**
	 * [__construct Initiates the chat model and the related table names].
	 */
	public function __construct()
	{
		parent::__construct();
		$this->table = 'chats';
		$this->chat_room_table = 'chat_rooms';
		$this->association_table = 'user_rooms_association';
	}

	/**
	 * [get_all_rooms Fetches all the rooms from the chat room table and returns the result array]
	 * @return [type] [description]
	 */
	public function get_all_rooms(){ 
		$this->db->from($this->chat_room_table);
		return $this->db->get()->result_array();
	}

	/**
	 * [get_messages Fetches all the messages for a chat room table and returns the result array]
	 * @param  [type] $chat_room_id [description]
	 * @return [type]               [description]
	 */
	public function get_messages( $chat_room_id ){ 
		$this->db->select('c.*, users.name as user_name')
		->from("$this->table as c" )
		->where('chat_room', $chat_room_id )
		->join('users', 'users.id = created_by' );
		return $this->db->get()->result_array();
	}

	/**
	 * 
	 * @param [str] $room_name [Room name to assign to the newly created chat room]
	 * @param [int] $user_id   [user, who is creating the chat room]
	 */
	public function add_room( $room_name, $user_id){
		$data = array(
			'name'       => $room_name,
			'created_by'      => $user_id,
			'created_at' => date('Y-m-j H:i:s'),
		);
		
		return $this->db->insert( $this->chat_room_table, $data );
	}

	/**
	 * [add_message adds the messages to the current chat room ]
	 * @param [int] $room_id [room id, to which the message has been posted]
	 * @param [str] $message [message to be posted]
	 * @param [int] $user_id [User posting the message]
	 */
	public function add_message( $room_id, $message, $user_id ){
		$data = array(
			'message'       => $message,
			'created_by'      => $user_id,
			'chat_room' 	=> $room_id,
			'created_at' => date('Y-m-j H:i:s'),
		);
		return $this->db->insert( $this->table, $data );
	}

	/**
	 * [join_room Attaches the user to the given chat room]
	 * @param  [int] $room_id [Room id to which user is being added]
	 * @param  [int] $user_id [current user id]
	 * @return [void]
	 */
	public function join_room( $room_id, $user_id ) {
		$association = $this->db->from($this->association_table)
		->where( 'user_id', $user_id)
		->get()->row();

		if( empty( $association) ) {
			$data = array(
					'user_id' => $user_id,
					'room_id' => $room_id
				);
			$this->db->insert( $this->association_table, $data);
		}
	}
}
