<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends MY_Controller {

	/**
	 * [__construct Loads the chat model and initiates a variable called userdata with the session key user_data ].
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model( 'chat_model' );
		$this->userdata = $this->session->userdata('user_data');
	}

	/**
	 * [index Index page for /chat, renders the chat rooms list]
	 * @return [void]
	 */
	public function index() {
		$rooms = $this->chat_model->get_all_rooms();
		$vars['react_vars'] = array( 'rooms' => $rooms );
		$this->render_view('chat/room_list', $vars );
	}

	public function add_room() {
		if( ! empty( $this->userdata ) ) {
			$room_name = $this->input->post( 'room_name' );
			if( ! empty( $room_name ) ) {
				$this->chat_model->add_room( $room_name, $this->userdata->id );
			}
		}
		$rooms = $this->chat_model->get_all_rooms();
		redirect( '/chat' );
	}

	/**
	 * [join_room Associates the current user to the chat room]
	 * @param  [int] $room_id [room id for the chat room]
	 * @return [void]
	 */
	public function join_room( $room_id ) {
		if( ! empty( $this->userdata ) ) {
			$this->chat_model->join_room( $room_id, $this->userdata->id );
			redirect( "/chat/room/$room_id" );
		}
		$rooms = $this->chat_model->get_all_rooms();
		$vars['react_vars'] = array( 'rooms' => $rooms );
		$this->render_view('chat/room_list', $vars );
	}

	/**
	 * [add_message Adds the message to the current chat room]
	 */
	public function add_message() {
		if( ! empty( $this->userdata ) ) {
			$room_id = $this->input->post( 'room_id' );
			$message = $this->input->post( 'message' );
			if( ! empty( $room_id ) ) {
				$this->chat_model->add_message( $room_id, $message, $this->userdata->id );
				redirect("/chat/room/$room_id");
			}
			redirect("/chat");
		}
	}

	/**
	 * [room Fetches the messages related to the given room and renders it]
	 * @param  [int] $room_id [GIven room id for which messages to show]
	 * @return [void]
	 */
	public function room( $room_id ) {
		$messages = $this->chat_model->get_messages( $room_id );
		$vars['react_vars'] = array( 'messages' => $messages, 'room_id' => $room_id );
		$this->render_view( 'chat/room', $vars );
	}
}
