<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	/**
	 * [__construct Loads the user model and the MY_controller's construct method]
	 */
	public function __construct() {
		parent::__construct();
		$this->load->model( 'user_model' );
	}

	/**
	 * [index Route for the index page /user ]
	 * @return [void] [renders the login page, with the given partial view path ].
	 */
	public function index()
	{
		$this->render_view( 'user/login' );
	}

	/**
	 * [login Logs in the person, if the credentials are supplied with POST request, renders the login page otherwise].
	 * @return [void] [renders the login page if get request, redirects to chat page, if user s verified]
	 */
	public function login() 
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if( $this->user_model->validate_login( $email, $password ) ) {
			$this->session->set_userdata( 'user_data', $this->user_model->get_by_email($email) );
			redirect('/chat');
		} else {
			$this->render_view( 'user/login' );
		}
	}

	/**
	 * [logout Logs out the current user and removes it from session ].
	 * @return [void] [redirects to the login page]
	 */
	public function logout() 
	{
		if( $this->session->has_userdata('user_data' ) ) {
			$this->session->unset_userdata('user_data');
		}
		redirect('/user/login');
	}

	/**
	 * [register Registers the user if it is a POST request with required data, renders the registration page otherwise].
	 * @return [void]
	 */
	public function register()
	{
		$data  = $this->input->post();
		if( ! empty( $data ) ) {
			$user = $this->user_model->get_by_email($data['email']);
			if( empty( $user ) ) {
				$this->user_model->create( $data );
				$data['message'] = 'New user has been created for' . $data['email'] .' Please login to continue'; 
			} else {
				$data['message'] = 'User already exists. Please use forgot password to continue';
			}
			$this->render_view( 'user/registration', $data );
		}
		$data['message'] = '';
		$this->render_view( 'user/registration', $data );
	}
}
