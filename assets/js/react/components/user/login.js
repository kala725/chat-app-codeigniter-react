import AppContainer from "../../AppContainer"

class UserLogin extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<AppContainer>
				<form method="POST" action="/user/login">
					<div className="form-group">
						<label htmlFor="login">Login</label>
						<input type="email" className="form-control" name="email" placeholder="Enter your email" required />
					</div>
					<div className="form-group">
						<label htmlFor="password">Password</label>
						<input type="password" className="form-control" name="password" placeholder="Enter Password" minLength="4" required />
					</div>
					<button type="submit" className="btn btn-primary">Login</button>
					<span> or </span>
					<a href="/user/register" className="btn btn-primary">Register</a>
				</form>
			</AppContainer>
		);
	}
}
export default UserLogin