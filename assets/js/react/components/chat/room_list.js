import AppContainer from "../../AppContainer"

class ChatRoomList extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		var rooms = []
		for (var key in this.props.rooms) {
		  if (this.props.rooms.hasOwnProperty(key)) {
		  	var room = this.props.rooms[key]
		  	var room_html = (
		  			<div className="row" key={room.id}>
		  				<div className="col col-md-3">
			  				{room.name}
			  			</div>
			  			<div className="col col-md-9">
			  				<a className="btn" href={"/chat/join_room/" + room.id }>
			  					Join Room
			  				</a>
			  			</div>
			  		</div>
		  		)
		  	rooms.push(room_html);
		  }
		}
		return (
			<AppContainer>
				<div className="form-group">
					<h3>Available Chat Rooms are</h3>
					{ rooms }
				</div>
				<span> OR </span>
				<form method="POST" action="/chat/add_room">
					<div className="form-group">
						<label htmlFor="login">Create a Room</label>
						<input type="text" className="form-control" name="room_name" minLength="4" placeholder="Enter the room name you want to create" required />
					</div>
					<button type="submit" className="btn btn-primary">Login</button>
				</form>
			</AppContainer>
		);
	}
}

export default ChatRoomList