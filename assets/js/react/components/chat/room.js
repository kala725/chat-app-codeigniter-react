import AppContainer from "../../AppContainer"

class ChatRoom extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		var messages = []
		for (var key in this.props.messages) {
		  if (this.props.messages.hasOwnProperty(key)) {
		  	var message = this.props.messages[key]
		  	var message_html = (
		  			<div className="row" key={message.id}>
		  				<div className="col col-md-3">
			  				{message.user_name} -
			  			</div>
		  				<div className="col col-md-9">
			  				{message.message}
			  			</div>
			  		</div>
		  		)
		  	messages.push(message_html);
		  }
		}
		return (
			<AppContainer>
				<div className="messages">
					{ messages }
				</div>
				<form method="POST" action="/chat/add_message">
					<div className="form-group">
						<label htmlFor="login">Enter message</label>
						<input type="text" className="form-control" name="message" minLength="4" placeholder="Enter the message" required />
						<input type="hidden" name="room_id" minLength="4" placeholder="Enter the message" value={this.props.room_id} />
					</div>
					<button type="submit" className="btn btn-primary">Post</button>
				</form>
			</AppContainer>
		);
	}
}

export default ChatRoom